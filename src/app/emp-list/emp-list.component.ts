import { Component, OnInit, QueryList, ViewChildren, Input, ViewChild, OnDestroy } from '@angular/core';
import { CountryService } from './country.service';
import { NgbdSortableHeader, SortEvent } from './sortable.directive';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../login/user.service';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';
import { toFormData } from './to-formdata';
@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <!-- <h4 class="modal-title">Hi there!</h4> -->
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <!-- <p>Hello, {{name}}!</p> -->
      <form style="height:100%"  [formGroup]="empForm">
            <label style="width:30%">Name</label>
            <input formControlName="name" type="text" placeholder="Name" style="width: 50%;" required>
            <span class="text-danger"
                *ngIf="(empFormControl.name.touched || submitted) && empFormControl.name.errors?.required">
                Name is required
            </span>
            <br>
            <label style="width:30%">Email</label>
            <input formControlName="email" type="email" placeholder="Email" style="width: 50%;" required>
            <span class="text-danger"
                *ngIf="(empFormControl.email.touched || submitted) && empFormControl.email.errors?.required">
                Email is required
            </span>
            <br>
            <label style="width:30%">Mobile No.</label>
            <input formControlName="contact" type="text" placeholder="Mobile No." style="width: 50%;" required>
            <span class="text-danger"
                *ngIf="(empFormControl.contact.touched || submitted) && empFormControl.contact.errors?.required">
                contact is required
            </span>
            <br>
            <label style="width:30%">Designation</label>
            <select formControlName="designation" type="text" placeholder="Designation" style="width: 50%;" required>
            <option value="hr">HR</option>
            <option value="maneger">Manager</option>
            <option value="sales">Sales</option>
            </select>
            <span class="text-danger"
                *ngIf="(empFormControl.designation.touched || submitted) && empFormControl.designation.errors?.required">
                designation is required
            </span>
            <br>
            <label style="width:30%">Gender</label>
            <label>
              <input type="radio" value="Male" formControlName="gender">
                <span>male</span>
            </label>
            <label>
              <input type="radio" value="Female" formControlName="gender">
                <span>female</span>
            </label>
            <span class="text-danger"
                *ngIf="(empFormControl.gender.touched || submitted) && empFormControl.gender.errors?.required">
                gender is required
            </span>
            <br>

            <label style="width:30%">Course</label>
            <label>
              <input type="checkbox" value="mca" formControlName="course">
                <span>MCA</span>
            </label>
            <label>
              <input type="checkbox" value="bca" formControlName="course">
                <span>BCA</span>
            </label><br>
            <!-- <input type="checkbox" ng-model="myVar"> -->
            <label style="width:30%">Image</label>
            <input (change)="onFileChange($event)" type="file" placeholder="Image" style="width: 50%;" accept=".png, .jpg"><br>
            <!-- <input type="button" value="Login"><br> -->
            
            <image-cropper [imageChangedEvent]="imageChangedEvent" [maintainAspectRatio]="true"
                            [aspectRatio]="4 / 4" [resizeToWidth]="300" [resizeToHeight]="300" [cropperMinWidth]="128"
                            [onlyScaleDown]="true" [roundCropper]="false" [alignImage]="'left'"
                            [style.display]="showCropper ? null : 'none'" format="png"
                            (imageCropped)="imageCropped($event)" (imageLoaded)="imageLoaded()"></image-cropper>
        </form>
    </div>
    <div class="modal-footer">
    <input type="button" value="Submit" (click)="submit()"><br>
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent implements OnInit {
  @Input() name: any;
  empForm: FormGroup;
  submitted: boolean = false;
  image!: File;
  imageChangedEvent: any;
  showCropper: boolean = false;
  croppedImage: string | null | undefined;
  constructor(public activeModal: NgbActiveModal, private _formBuilder: FormBuilder, private userService: UserService) {
    this.empForm = this._formBuilder.group({
      _id: [null],
      name: [null, Validators.required],
      email: [null, Validators.required],
      contact: [null, Validators.required],
      designation: [null, Validators.required],
      gender: [null, Validators.required],
      course: [null, Validators.required],
      image: [null, Validators.required],
    })
  }
  ngOnInit() {
    if (this.name) {
      this.empForm.patchValue(this.name);
    }
  }
  get empFormControl() {
    return this.empForm.controls;
  }
  onFileChange(event: any) {
    this.imageChangedEvent = event;
  }

  imageLoaded() {
    this.showCropper = true;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.empForm.patchValue({
      image: base64ToFile(event.base64 ? event.base64 : "")
    });
  }

  submit() {
    this.submitted = true;
    if (this.empForm.valid) {
      if (this.name) {
        this.empForm.patchValue({
          _id: this.name._id
        })
        this.userService.updateEmp(toFormData(this.empForm.value)).subscribe((data) => {
          this.activeModal.dismiss();
        })
      }
      else {
        this.userService.saveEmp(toFormData(this.empForm.value)).subscribe((data) => {
          this.activeModal.dismiss();
        })
      }

    }
  }

}

@Component(
  {
    selector: 'app-emp-list',
    templateUrl: './emp-list.component.html',
    styleUrls: ['./emp-list.component.scss'],
  }
)
export class EmpListComponent implements OnInit {
  employees: any;
  searchTxt: string = "";
  @ViewChildren(EmpListComponent) headers!: QueryList<NgbdSortableHeader>;
  filterData: any;
  total: number = 0;
  constructor(
    public service: CountryService,
    public uService: UserService,
    private modalService: NgbModal
  ) {
    this.getEmp();
  }
  dtOptions: DataTables.Settings = {};
  open() {
    const modalRef = this.modalService.open(NgbdModalContent, { size: 'lg' });
    modalRef.result.then((data) => {
    }, (reason) => {
      this.getEmp();

    });
  }
  openEdit(id: string) {
    const modalRef = this.modalService.open(NgbdModalContent, { size: 'lg' });
    modalRef.componentInstance.name = id;
    modalRef.result.then((data) => {
    }, (reason) => {
      this.getEmp();
    });
  }
  changeActive(emp: any, active: any) {
    emp.active = active.target.value;
    this.uService.setActive(emp).subscribe((data) => {
      console.log(data)
    })
  }
  getEmp() {
    this.uService.getEmp().subscribe((data) => {
      this.employees = data;
      this.search("");
    })
  }
  deleteEmp(emp: any) {
    this.uService.deleteEmp(emp).subscribe((data) => {
      this.getEmp();
    })
  }
  updateEmp(emp: any) {
    this.uService.updateEmp(emp).subscribe((data) => {
      this.getEmp();
    })
  }
  search(term: string) {
    if (!term) {
      this.filterData = this.employees;
      this.sliceDate();
      this.total = this.filterData.length;
    } else {
      this.filterData = this.employees.filter((x: any) =>
        x.name.trim().toLowerCase().includes(term.trim().toLowerCase()) || x.email.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.sliceDate();
      this.total = this.filterData.length;
    }
  }
  sliceDate() {
    this.filterData.forEach((element: any) => {
      element.date = element.date.slice(0, 10)
    });
  }
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      searching: false,
      lengthChange: false,
      'columnDefs': [{
        orderable: false, targets: 1
      },
      {
        orderable: false, targets: 4
      },
      {
        orderable: false, targets: 5
      },
      {
        orderable: false, targets: 6
      },
      {
        orderable: false, targets: 7
      },
      {
        orderable: false, targets: 10
      }
    ],
    };
  }

  getBaseUrl() {
    return 'http://localhost:2000'
  }
}