import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  submitted: boolean = false;
  constructor(
    private router: Router,
    private _formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.userForm = this._formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }
  get userFormControl() {
    return this.userForm.controls;
  }
  login() {
    this.submitted = true;
    if (this.userForm.valid) {
      this.userService.login(this.userForm.value).subscribe((data) => {
        // console.log(data);
        this.router.navigate(['/dashboard']);

      })
    }
  }


}
