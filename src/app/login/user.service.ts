import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { throwError, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    baseUrl: string = 'http://localhost:2000'
    constructor(
        private _http: HttpClient
    ) { }
    login(userForm: any): Observable<any> {
        return <Observable<any>>this._http
            .post(this.baseUrl + "/api/login", userForm)
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    saveEmp(emp: any): Observable<any> {
        return <Observable<any>>this._http
            .post(this.baseUrl + "/api/employee", emp)
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    getEmp(): Observable<any[]> {
        return <Observable<any>>this._http
            .get(this.baseUrl + "/api/getEmployee")
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.message : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    deleteEmp(emp: any): Observable<any> {
        return <Observable<any>>this._http
            .post(this.baseUrl + "/api/deleteEmployee", emp)
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    setActive(emp: any): Observable<any> {
        return <Observable<any>>this._http
            .post(this.baseUrl + "/api/activeEmployee", emp)
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    updateEmp(emp: any): Observable<any> {
        return <Observable<any>>this._http
            .post(this.baseUrl + "/api/updateEmployee", emp)
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    getUser(): Observable<any[]> {
        return <Observable<any>>this._http
            .get(this.baseUrl + "/api/currentUser")
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.message : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
    logout(): Observable<any> {
        return <Observable<any>>this._http
            .get(this.baseUrl + "/api/logout")
            .pipe(
                catchError((err) => {
                    alert(err.error ? err.error.errors : 'Error occurred while api call');
                    return throwError(err);    //Rethrow it back to component
                })
            )
    }
}