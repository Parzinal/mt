import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../login/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  user:any;
  constructor(
    private userService: UserService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe((user) => {
      // console.log(user);
      this.user = user;
    })
  }
  logout() {
    this.userService.logout().subscribe((data)=> {
      console.log(data);
      this.router.navigate(['/'])
    })
  }

}
