import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../login/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private userService : UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe((user)=> {
      // console.log(user);
      if(user.message=="not found")  {
        this.router.navigate([''])
        alert("Log in required, redirecting to login page")
      }
    })
  }
}
